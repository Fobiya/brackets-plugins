## Brackets-plagins

### List modules

* accessability [- link](https://github.com/konstantinkobs/brackets-colorHints)
* colorHints  [- link](https://github.com/konstantinkobs/brackets-colorHints)
* CSSFier [- link](https://caferati.me/labs/cssfier)
* Emmet [- link](https://emmet.io/)
* Js-Beautify Ochou <mfkddcwy@gmail.com>  [- link](https://emmet.io/)
* jsbeautifier  <ryushi@gmail.com>  [- link](https://github.com/taichi/brackets-jsbeautifier) 
* Markdown Preview [- link](https://github.com/gruehle/MarkdownPreview) 
* Minifier http://wylst.com
* Special Html Characters [- link](https://github.com/thaneuk/brackets-special-html-chars) 
* Brackets Git <Martin_Zagora> [- link](https://github.com/brackets-userland/brackets-git)

### Template

* Mushin Dark [- link](https://github.com/Brackets-Themes/MushinDark) 
 


